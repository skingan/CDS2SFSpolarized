# CDS2SFSpolarized.pl
Calculates site frequency spectra of synonymous and nonsynonymous sites for protein coding alignment.
Alignment must include a population same and two outgroup sequences, a near and a far outgroup, which 
are used to polarized divergent sites between the near outgroup and the focal population.

# Author:
Sarah B. Kingan

Lummei Analystics

28 March 2016

# Input: 
One or more files containing multiple alignments of CDS in fasta format.

Formatting requirements for alignment:
1. sequence must be coding (length is multiple of 3)
2. all sequences must be the same length
3. outgroup sequence identified with 'near_outroup' and 'far_outgroup' in header line
4. files must have ".fa" extension


# Output:
			
1. STOUT
tab delimited text with the following fields (where N is the number of ingroup samples):

		filename_Syn S1 S2 ... SN
		
		filename_NonSyn S1 S2 ... SN
		
2. STDERR
progress reports, alignment information, details about excluded sites


# Example Calls:	

SINGLE FILE:

	CDS2SFSpolarized.pl mydata.fa > mydata_sfs.out 2> mydata_sfs.err

ALL FILES IN DIRECTORY:

	CDSS2SFSpolarized.pl > mydata_sfs.out 2> mydata_sfs.err


# Exclusion of sites/handling of missing data:
1. codons with any character other than AGCT in any sample are excluded in all samples.
2. codons with sites that have more than 2 states in the ingroup sequences are excluded (violation of infinite sites model).
3. codons where ancestral state of the ingroup sequences cannot be unanbiguously inferred are ommited.
4. codons where the ingroup has more than two states are excluded.
5. codons that are a stop codon in any sample are ignored across the entire alignment. Output for alignments with premature
stop codons are flagged with a "#" at the begining of the ouput line.

# Revision History:

Forked from CDS2SFS.pl 
https://github.com/skingan/CDS2SFS.git
